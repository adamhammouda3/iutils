/* timerInterrupter: implementation of interrupter.h using timers
 *
 * Author: Stephen F. Siegel
 * Last modified: 05-Nov-2012
 */
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
//#include <mpe.h>
#include <signal.h>
#include <sys/time.h>
#include "rantools.h"
#include "interrupter.h"

/* 10^6 */
#define MILLION 1000000

/* 10^-6 */
#define ONE_MILLIONTH .000001

/* 10^9 */
#define BILLION 1000000000

static const double rand_range = (double)RAND_MAX + 1.0;

static int id;
static int numInterrupts = 0;
static int seed;
static int iLimit;
static int parity;
static double duration;
static double mean;
static double stddev;

static double sync;

// timers and timeofday require timevals (10^-6 precision)...

/* struct itimerval {
 *    struct timeval it_interval; //Period b/w succesive timer interrupts (if 0, interrupt occurs 1xs)
 *    struct timeval it_value;    //Period b/w now and the 1st interrupt (if 0, alarm is disabled)
 * } in <sys/time.h>
 */
static struct itimerval timer;

/*  struct timeval {
 *     time_t       tv_sec   //seconds
 *     suseconds_t  tv_usec  //microseconds
 *  } //in <sys/time.h>
 */
static struct timeval start_tv, stop_tv;
static struct timeval timer_interval, timer_value;

// nanosleep requires a timespec (10^-9 precision)...

/* struct timespec {
 *    time_t tv_sec; //seconds
 *    long tv_nsec;  //nanoseconds
 * }
 */
static struct timespec interrupt_duration, remaining_time;

/* Measure difference between two time values, returning answer in
 * seconds as double. */
static double delta(struct timeval *tp1, struct timeval *tp2) {
  time_t sec1 = tp1->tv_sec, sec2 = tp2->tv_sec;
  suseconds_t microsec1 = tp1->tv_usec, microsec2 = tp2->tv_usec;

  if (microsec2 > microsec1) {
    microsec1 += MILLION;
    sec1--;
  }
  return ((double)(MILLION*(sec1-sec2) + microsec1 - microsec2))/1000000.0;
}

/* Sets the components of the time spec pointed to by ts
 * to correspond to the double value time. */
static void setTimeSpec(double time, struct timespec *ts) {
  long numSeconds = (long)time;

  ts->tv_sec = numSeconds;
  ts->tv_nsec = (long)((time - numSeconds)*BILLION);
}

static void setTimeVal(double time, struct timeval *tv) {
  long numSeconds = (long)time;
  
  tv->tv_sec = numSeconds;
  tv->tv_usec = (long)((time - numSeconds)*MILLION);
  //printf("Time setting:  us=%ld, sec=%ld\n",(time - numSeconds)*MILLION,numSeconds);
}

/* Alarm arrived.  wait for duration.  then choose period
 * and set next alarm. */
static void handler(int signum) {
  int i, rc;
  double r = 0.0;
  double period;
  if (signum != SIGALRM) {
    fprintf(stderr,
	    "Proc %d: caught unxpected signal: %d\n",
	    id, signum);
    fflush(stderr);
    exit(1);
  }
#ifdef DEBUG
  printf("Proc %d: interrupt %d: sleeping for %.3es.\n",
	 id, numInterrupts, duration);
  fflush(stdout);
#endif
  
  rc = nanosleep(&interrupt_duration, &remaining_time);
  numInterrupts++;

  if (rc) {
    // where is errno???  check it is EINTR.
    fprintf(stderr, "Proc %d: received signal %d\n", id, rc);
    fflush(stderr);
    exit(1);
  }
  
#ifdef PI_GAUS
  period = rng(mean,stddev);
#elif PI_CNST
  period = mean;
#endif
  
  if (period < ONE_MILLIONTH) period = ONE_MILLIONTH;
  
  if(numInterrupts>=iLimit)
    setTimeVal(0.0, &timer.it_value);
  else
    setTimeVal(period, &timer.it_value);

#ifdef DEBUG
  printf("Proc %d: going back to work.  Will sleep again in %.3es.\n",
	 id, period);
  fflush(stdout);
#endif
  rc = setitimer(ITIMER_REAL, &timer, NULL);
  // what if timer goes off before this function returns?
  if (rc) {
    fprintf(stderr, "Proc %d: setitimer returned %d on time %.3e\n",
	    id, rc, period);
    fflush(stderr);
    exit(rc);
  }
}

void initInterrupter(char* config_file) {
  int rc;
  struct sigaction sa;
  
#ifdef DEBUG
  printf("Initializing interrupter\n");
  fflush(stdout);
#endif
  timer.it_interval.tv_sec = 0;
  timer.it_interval.tv_usec = 0;
  sa.sa_handler = handler;
  sigemptyset(&sa.sa_mask);
  sa.sa_flags = 0;
  rc = sigaction(SIGALRM, &sa, NULL);
  if (rc) {
    fprintf(stderr,
	    "Proc %d: failed to register signal action: rc=%d\n",
	    id, rc);
    fflush(stderr);
    exit(rc);
  }
}


void setInterrupterParameters(int _id, int _seed, double _duration,
			      double _mean, double _stddev,
			      int _iLimit, int _parity, double _sync) {
#ifdef DEBUG
  printf("Setting interrupter parameters: id=%d, seed=%d, "
	 "duration=%.3es, mean=%.3es, stddev=%.3es, nbuf=%d,iLimit=%d,parity=%d\n",
	 _id, _seed, _duration, _mean, _stddev, _iLimit,_parity);
  fflush(stdout);
#endif
  id = _id;
  seed = _seed;
  duration = _duration;
  mean = _mean;
  stddev = _stddev;
  iLimit=_iLimit;
  parity = _parity;
  sync=_sync;
  
  setTimeSpec(duration, &interrupt_duration);
  setTimeSpec(0.0, &remaining_time);
}

void startInterrupter() {
  int rc;
  double period;

  gettimeofday(&start_tv, NULL);
#ifdef DEBUG
  printf("Proc %d starting interruption mechanism.\n", id);
  fflush(stdout);
#endif
  srand(seed);

#ifdef P0_UNI
  // first period selected from constant disrtibution so
  // starting point is evenly distributed...
  period =  2*mean*(((double)rand())/rand_range);
#elif P0_CNST
  period = mean;
#elif P0_OFFSET
  //period = (id % 2) ? mean : (sync*mean);
  /*below requires that sync \in [0,1]*/
  period = (id % 2) ? mean : mean + (sync*duration);
#endif
  
  if (period < ONE_MILLIONTH)
    period = ONE_MILLIONTH;
#ifdef DEBUG
  frintf(stdout,"1PER: Proc %d: rand_rang=%lf,"
	 "mean=%lf, period=%lf\n",id,rand_range,mean,period);
#endif
  
  //Activate parity setting
  if ((id+1)%parity == 0)
    setTimeVal(period, &timer.it_value);
  else if ((id+1)>2)
    setTimeVal(0.0, &timer.it_value);
  
#ifdef DEBUG
  printf("Proc %d: first interruption will occur in %.3es.\n",
	 id, period);
  fflush(stdout);
#endif
  rc = setitimer(ITIMER_REAL, &timer, NULL);
  if (rc) {
    fprintf(stderr, "Proc %d: setitimer returned %d on time %.3es.\n",
	    id, rc, period);
    fflush(stderr);
    exit(rc);
  }
}

void stopInterrupter() {
  int rc;
  gettimeofday(&stop_tv, NULL);
  setTimeVal(0.0, &timer.it_value);
  setitimer(ITIMER_REAL, &timer, NULL);
  
}

double getDuration() {
  return duration;
}

double getMean() {
  return mean;
}

double getStddev() {
  return stddev;
}

/* Return total number of interrupts */
int getNumInterrupts() {
  return numInterrupts;
}

/* Returns total time of interrupts */
double getTotalInterruptTime() {
  return numInterrupts*duration;
}

/* Returns total time, in seconds between call to startInterrupter
 * and call to stopInterrupter. */
double getTotalTime() {
  return delta(&stop_tv, &start_tv);
}

void cleanInterrupter(){
  //fprintf(stderr,"Proc %d Cleaning r-buf... \n",id);
#ifdef SETRAND
  //free(ranbuf);
#endif
}
