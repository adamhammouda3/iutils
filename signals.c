/* 
   Signaling example.  Uses following library routines:
   
   In header signal.h:
   
   kill (pid_t pid, int signum);
   
   We use se signums SIGSTOP, SIGCONT, and SIGTERM.
   
   In header time.h:
   
   int nanosleep (const struct timespec *requested_time, struct
   timespec *remaining);
   
   members of struct timespec: long int tv_sec: This represents the
   number of whole seconds of elapsed time.
   
   long int tv_nsec: This is the rest of the elapsed time (a fraction
   of a second), represented as the number of nanoseconds. It is
   always less than one billion.
   
   In header unistd.h:
   
   pid_t fork(void): 
   
   pid_t getppid(void); // get parent pid
*/
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>

const double rand_range = (double)RAND_MAX + 1.0;
const double BILLION = 1000000000.0;
int rank;
int nprocs;

int main(int argc, char *argv[]) {
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  int child = fork();

  srand((rank+1)*1234);
  if (child == 0) {
    // I am child, the "interrupter" (just like in real life)
    int parent = getppid();
    struct timespec interrupt_duration; // duration of one interrupt
    struct timespec interrupt_period; // time until next interrupt
    struct timespec remaining_time;

    // set duration of 3 seconds...
    interrupt_duration.tv_sec = 3;
    interrupt_duration.tv_nsec = 0;
    while (1) {
      // random # in [0,10)
      double r = 10.0*(((double)rand())/rand_range);
      int secs = (int)r;
      int err;
      
      interrupt_period.tv_sec = (long)secs;
      interrupt_period.tv_nsec = (long)(BILLION*(r-secs));
      printf("Interrupter %d: sleeping for %.3f seconds\n", rank, r);
      fflush(stdout);
      nanosleep(&interrupt_period, &remaining_time);
      printf("Interrupter %d woke and is sending SIGSTOP to parent.\n", rank);
      fflush(stdout);
      err = kill(parent, SIGSTOP);
      if (err != 0) {
	printf("Interrupter %d failed to send SIGSTOP with error code %d\n",
	       rank, err);
	fflush(stdout);
      }
      printf("Interrupter %d: sleeping for %.3f seconds\n", rank, 3.0);
      fflush(stdout);
      nanosleep(&interrupt_duration, &remaining_time);
      printf("Interrupter %d woke and is sending SIGCONT to parent.\n", rank);
      fflush(stdout);
      err = kill(parent, SIGCONT);
      if (err != 0) {
	printf("Interrupter %d failed to send SIGCONT with error code %d\n",
	       rank, err);
	fflush(stdout);
      }
    }
    printf("Interrupter %d terminating.\n", rank);
    fflush(stdout);
  } else {
    // I am parent
    int i, j, sum = 1, err;
    
    for (i=0; i< 500; i++) {
      printf("Proc %d running with i=%d, sum=%d\n", rank, i, sum);
      fflush(stdout);
      for (j=1; j<12345678; j++)
	sum  = (sum + j)%1000;
    }
    printf("Proc %d sending SIGTERM to interrupter\n", rank);
    fflush(stdout);
    err = kill(child, SIGTERM);
    if (err != 0) {
      printf("Proc %d failed to send SIGTERM with error code %d", rank, err);
      fflush(stdout);
    }
    printf("Proc %d terminating.\n", rank);
    fflush(stdout);
  }
  MPI_Finalize();
  return 0;
}
