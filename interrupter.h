#include "rantools.h"

/* interrupter.h: interface for injecting random pauses into a
 * process.
 *
 * Author: Stephen F. Siegel
 * Last modified: 05-Nov-2012
 *
 * Use this by linking an implementation with your program.
 * Start by calling initInterrupter to specify the parameters
 * of your interrupter.  Each interruption will be for a 
 * fixed specified duration.  The period between interruptions
 * will be chosed randomly, based on a normal distribution.
 * The mean and standard deviation for the distribution are
 * specified.
 *
 * To start the interrupter, invoke startInterrupter.
 * To stop it, call stopInterrupter.
 * Once stopped, it cannot be re-started (for now---might
 * improve this in the future).
 * After it is stopped, you can get some basic statistics
 * about the session from the functions provided.
 */

/* Initialize the interrupter. Do this early (for example, before
 * MPI_Init, before any mallocs).  This does not start the
 * interrupter.  After this, one must call
 * setInterrupterParameters(...) and then startInterrupter(). */
void initInterrupter();

/* Sets the parameter values.  The arguments are:
 * 
 * (1) an id number used for debugging
 * (2) a seed for the random number generator
 * (3) the duration (in seconds) of each interruption
 * (4) the mean time between interruptions, and
 * (5) the standard deviation in the normal distribution of time between
 *     interruptions.
 * (6) iLimit -
 * (7) parity -
 * (8) sync   -
 */           
void setInterrupterParameters(int id, int seed, double duration,
			      double mean, double stddev,
			      int _iLimit,int parity,double _sync);

/* Starts the interrupter. */
void startInterrupter();

/* Stops the interrupter.  Can only be called after startInterrupter.
 * Once stopped, it cannot be re-started.  */
void stopInterrupter();

/* Returns the duration (in seconds) specified in initInterrupter. */
double getDuration();

/* Returns the mean (in seconds) specified in initInterrupter. */
double getMean();

/* Returns the standard deviation specified in initInterrupter. */
double getStddev();

/* Return total number of interruptions that occurred during
 * the session.  Can only be called after stopInterrupter. */
int getNumInterrupts();

/* Returns total time of interruptions.  Can only be called
 * after stopInterrupter.  */
double getTotalInterruptTime();

/* Returns total time, in seconds between call to startInterrupter
 * and call to stopInterrupter.  Can only be called after 
 * stopInterrupter.  */
double getTotalTime();

/* Free any extraneous memory or processes associated with the interrupter */
void cleanInterrupter();
