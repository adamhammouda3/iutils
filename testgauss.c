#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include<assert.h>

#define ranf() rand()/RAND_MAX

/*
 *  compute a gaussian random number of mean m and stddev s 
 *  must set seed separately with srand()
 */
static float rng(float m, float s){        
  float x1, x2, w, y1;

  do {
    x1 = 2.0 * ranf() - 1.0;
    x2 = 2.0 * ranf() - 1.0;
    w = x1 * x1 + x2 * x2;
  } while ( w >= 1.0 );
    
  w = sqrt( (-2.0 * log( w ) ) / w );
  y1 = x1 * w;
  
  return( m + y1 * s );
}

/*
 *  compute a buffer of n guassian random numbers
 *  using box-muller transformation
 *  memory allocated internally. follow by free_rnglst
 */
static float *rnglst(float m, float s, size_t n){
  int i;
  float *ranbuf;
  assert(ranbuf = malloc(sizeof(float)*n));
  for (i=0;i<n;++i)
    ranbuf[i] = rng(m,s);
  return ranbuf;
}

/* 
 * free memory allocated in rnglst function
 */
static void free_rnglst(float *list){
  free(list);
}

/*
int main(){
  int i, n=10;
  srand(time(NULL));
  float x = rng(0.0,1.0);
  printf("x: %f\n", x);
  float *y = rnglst(0.0,1.0,n);
  for (i=0;i<n;++i)
    printf("%f ", y[i]);
  printf("\n");
  free_rnglst(y);
  return 0;
}
*/
