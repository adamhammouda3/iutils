#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <assert.h>

/* Prints usage information and exits with return status 1. */
static void quit() {
  printf("Input file must have format:\n\n");
  printf("parity = <INTEGER>\n");
  printf("interruptLim = <INTEGER>\n");  
  printf("sync = <DOUBLE>\n");
  printf("delay_dur = <DOUBLE>\n");
  printf("delay_per_mean = <DOUBLE>\n");
  printf("delay_per_stddev = <DOUBLE>\n");
  fflush(stdout);
  exit(1);
}

/* Reads a key-value pair from a file, where the value is an integer.
 * Takes as input a file pointer, a key (string), and a pointer to an
 * integer.  The text pointed to in the file must have the form "key =
 * VALUE".  This function checks that the key read matches the given
 * key; if they fail to match, an error is reported and execution
 * halts.  It then parses the integer and stores the resulting integer
 * value in the block pointed to by the given integer pointer.  Errors
 * are reported if anything does not parse.
 */
static void readint(FILE *file, char *keyword, int *ptr) {
  char buf[101];
  int value;
  int returnval;

  returnval = fscanf(file, "%100s", buf);
  if (returnval != 1) quit();
  if (strcmp(keyword, buf) != 0) quit();
  returnval = fscanf(file, "%10s", buf);
  if (returnval != 1) quit();
  if (strcmp("=", buf) != 0) quit();
  returnval = fscanf(file, "%d", ptr);
  if (returnval != 1) quit();
}

static void readlong(FILE *file, char *keyword, long long int *ptr) {
  char buf[101];
  long value;
  int returnval;

  returnval = fscanf(file, "%100s", buf);
  if (returnval != 1) quit();
  if (strcmp(keyword, buf) != 0) quit();
  returnval = fscanf(file, "%10s", buf);
  if (returnval != 1) quit();
  if (strcmp("=", buf) != 0) quit();
  returnval = fscanf(file, "%lld", ptr);
  if (returnval != 1) quit();
}


/* Read a key-value pair from a file, where the value is a double
   precision floating point number.   See comments for readint.
*/
static void readdouble(FILE *file, char *keyword, double *ptr) {
  char buf[101];
  int value;
  int returnval;

  returnval = fscanf(file, "%100s", buf);
  if (returnval != 1) quit();
  if (strcmp(keyword, buf) != 0) quit();
  returnval = fscanf(file, "%10s", buf);
  if (returnval != 1) quit();
  if (strcmp("=", buf) != 0) quit();
  returnval = fscanf(file, "%lf", ptr);
  if (returnval != 1) quit();
}

void readConfigInterrupter(char *infilename, int *parity,int *iLimit,
			  double *sync,
			  double *delay_dur, double *delay_per_mean,
			  double *delay_per_stddev){
  FILE *infile = fopen(infilename, "r");
  assert(infile);
  
  readint(infile, "parity", parity);
  readint(infile, "interruptLim", iLimit);
  readdouble(infile, "sync", sync);
  readdouble(infile, "delay_dur", delay_dur);
  readdouble(infile, "delay_per_mean", delay_per_mean);
  readdouble(infile, "delay_per_stddev", delay_per_stddev);
  printf("interrupter: "  
	 "parity=%d, interruptLim=%d, sync=%lf, "
	 "delay_dur=%lf, delay_per_mean=%lf, delay_per_stddev=%lf \n",
	 *parity, *iLimit, *sync,*delay_dur, *delay_per_mean, *delay_per_stddev);
  fflush(stdout);
  
  assert(*sync>=0);
}
