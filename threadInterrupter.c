/*
 * OBFUSCATED.  Using signalInterrupter instead.
 *
 * This is all non-portable stuff.
 *
 * To see what to do for a Mac, see:
 * https://developer.apple.com/library/mac/#releasenotes/Performance/RN-AffinityAPI/_index.html
 */
#define _XOPEN_SOURCE 600
#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <sched.h>
#include <pthread.h>
#include <unistd.h>
#include "interrupter.h"

#ifdef TIMEOFDAY
#define measureWorkTime measureWorkTime_timeofday
#else
#define measureWorkTime measureWorkTime_clock
#endif

#define TUNE_BOUND 1000000000

/* The number of tests that will be performed to determine
 * how precise the prediction of work time is */
#define NTESTS 20

static const double rand_range = (double)RAND_MAX + 1.0;
static int id;
static double duration;
static double mean;
static double stddev;
static struct timeval start_time;
static struct timeval stop_time;
static pthread_t thread;
static int numInterruptions = 0;
static int durationUnits; // number of work units for 1 duration
int sum;


static double delta(struct timeval *tp1, struct timeval *tp2) {
  time_t sec1 = tp1->tv_sec, sec2 = tp2->tv_sec;
  suseconds_t microsec1 = tp1->tv_usec, microsec2 = tp2->tv_usec;

  if (microsec2 > microsec1) {
    microsec1 += 1000000;
    sec1--;
  }
  return ((double)(1000000*(sec1-sec2) + microsec1 - microsec2))/1000000.0;
}


static void print_cpu_set(FILE *out, cpu_set_t *cpuset) {
  int j;
  int first = 1;

  fprintf(out, "{");
  for (j = 0; j < CPU_SETSIZE; j++) {
    if (CPU_ISSET(j, cpuset)) {
      if (first) {
	first = 0;
      } else {
	fprintf(out, ", ");
      }
      fprintf(out, "%d", j);
    }
  }
  fprintf(out, "}");
  fflush(out);
}

/* Prints set of CPUs on which the calling thread can run */
static void print_affinity(FILE *out) {
  cpu_set_t mask;
  int err = sched_getaffinity(0, sizeof(mask), &mask);
  
  if (err) {
    fprintf(out, "Proc %d: failed to get affinity for thread\n", id);
    fflush(out);
  }
  print_cpu_set(out, &mask);
  fflush(out);
}

/* Sets the CPU affinity of the calling thread to the given CPU.
 * The tid is used for diagnostic output only.  */
static void setAffinity(int tid, int cpuid) {
  cpu_set_t mask; 
  int error;

  CPU_ZERO(&mask); 
  CPU_SET(cpuid, &mask); 
  error = sched_setaffinity(0, sizeof(mask), &mask);
  if (error == 0) {
#ifdef DEBUG
    printf("Thread %d has been assigned to CPU %d\n", tid, cpuid);
    fflush(stdout);
#endif
  } else {
    printf("Thread %d failed to be assigned to CPU %d\n", tid, cpuid);
    fflush(stdout);
    exit(-1);
  }
}

// clock() is only accurate to 100-th of second on Chimera.

static double measureWorkTime_clock(int numWorkUnits) {
  int i;
  clock_t start_clock, stop_clock;
  double result;

  sum = 0;
  start_clock = clock();
  for (i=0; i<numWorkUnits; i++) sum = (sum + i)%100;
  stop_clock = clock();
  result= ((double)(stop_clock-start_clock))/(double)CLOCKS_PER_SEC;
#ifdef DEBUG
  printf("Proc %d: measureWorkTime: numWorkUnits=%d, time=%.3e\n",
	 id, numWorkUnits, result);
  fflush(stdout);
#endif
  return result;
}

static double measureWorkTime_timeofday(int numWorkUnits) {
  int i;
  struct timeval start_tv, stop_tv;
  double result;

  sum = 0;
  gettimeofday(&start_tv, NULL);
  for (i=0; i<numWorkUnits; i++) sum = (sum + i)%100;
  gettimeofday(&stop_tv, NULL);
  result = delta(&stop_tv, &start_tv);
#ifdef DEBUG
  printf("Proc %d: measureWorkTime: numWorkUnits=%d, time=%.3e\n",
	 id, numWorkUnits, result);
  fflush(stdout);
#endif
  return result;
}

/* Problems given a set of (x,y) which approximates a function,
 * find a best estimate of x_b of x which will tend to produce
 * a given goal y_goal.
 */
static void computeDurationUnits() {
  // x is current estimate of duration units
  int x = (TUNE_BOUND*duration)/measureWorkTime(TUNE_BOUND);
  // y is current measure of time to do x units
  double y;
  int total_x = 0;
  double total_y = 0.0;
  int i;

  for (i=0; i<NTESTS; i++) {
    y = measureWorkTime(x);
    total_x += x;
    total_y += y;
    x = (total_x*duration)/total_y;
  }
  durationUnits = x;
  printf("Proc %d: durationUnits=%d\n", id, durationUnits);
  fflush(stdout);
}

static double testWorkPrediction() {
  int i;
  double total_measured = 0.0;
  double mean_measured;
  double difference;

  for (i=0; i<NTESTS; i++) {
    double measured = measureWorkTime(durationUnits);

    total_measured += measured;
  }
  mean_measured = total_measured/NTESTS;
  difference = mean_measured - duration;
  printf("Proc %d: durationUnits=%d duration=%.3es "
	 "mean_measured=%.3es error=%.3es\n",
	 id, durationUnits, duration, mean_measured, difference);
  fflush(stdout);
}

void initInterrupter(int _id, int seed, double _duration, double _mean,
		     double _stddev) {
  int i;
  
  id = _id;
  duration = _duration;
  mean = _mean;
  stddev = _stddev;
  srand(seed);
  computeDurationUnits();
  testWorkPrediction();
}

void *run (void *arg) {
#ifdef DEBUG
  printf("Proc %d: interrupter thread is assigned to CPUs ", id);
  print_affinity(stdout);
  printf("\n");
  fflush(stdout);
#endif
  while (1) {
    // choose the random sleep time, sleep
    // wake up and add numbers
  }  
}

void startInterrupter() {
  int err;

#ifdef DEBUG
  printf("Proc %d: main thread is assigned to CPU(s) ", id);
  print_affinity(stdout);
  printf("\n");
  fflush(stdout);
#endif
  gettimeofday(&start_time, NULL);
  err = pthread_create(&thread, NULL, run, NULL);
  if (err) {
    fprintf(stderr, "Proc %d: Failed to create interrupter thread: error %d\n",
	    id, err);
    fflush(stderr);
    exit(1);
  }
}

void stopInterrupter() {
  int err;

  gettimeofday(&stop_time, NULL);
  err = pthread_cancel(thread);
  if (err) {
    fprintf(stderr, "Proc %d: warning: failed to cancel interrupter thread\n",
	    id);
    fflush(stderr);
  }
}

double getDuration() {
  return duration;
}

double getMean() {
  return mean;
}

double getStddev() {
  return stddev;
}

/* Return total number of interruptions */
int getNumInterruptions() {
  return numInterruptions;
}

/* Returns total time of interruptions */
double getTotalInterruptTime() {
  return numInterruptions * duration;
}

/* Returns total time, in seconds between call to startInterrupter
 * and call to stopInterrupter. */
double getTotalTime() {
  return delta(&stop_time, &start_time);
}

