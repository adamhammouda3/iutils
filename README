===================
About:
===================
Synthetic computational noise generating utilities for research into
noise resistance strategies.

===================
Users:
===================
Andrew Siegel
Stephen Siegel
Adam Hammouda

==============================================
Adding This Library To An Existing git Project
==============================================
# This is it! -- http://git-scm.com/book/en/v2/Git-Tools-Submodules#Starting-with-Submodules
$ git submodule add git@bitbucket.org:adamhammouda3/iutils.git

===================
Compiler directives
===================
-DDEBUG          Enables a number of introspecting print statements

-----------------------------------------------------------------
- Directives for the first "period before interruption"=period
-----------------------------------------------------------------
-P0_CNST 	 Keeps every processors initial period starts at the
		 same time.
-P0_UNI		 Every processors initial period is sampled from a 
		 uniform distribution seeded by it's rank.
-P0_OFFSET	 Every processors initial period is set to begin
		 by some offset of it's neighbors (in 'every-other' 
		 fashion).

-----------------------------------------------------------------
- Directives for every intermediate period afterward.
-----------------------------------------------------------------
-PI_GAUS	 Intermediate period is sampled from a gaussian
-PI_CNST	 Intermediate period is some constant


===================
Config File Format
===================
parity = 1		    /* Used to determine whether or not odd or even 
       	 		       procs will experience interrupts */
interruptLim = 2000 	    /* The limit on the number of interptions that can be 
	       		       experienced by any processor */
sync = 1.0     		    /* A variable which defines a period offset between
       			       adjacent processors. sync in range [1,2] for our 
			       interests. sync=alpha+1 */
delay_dur = 0.1		    /* Duration of slowdown */
delay_per_mean = 0.3	    /* Mean of slowdown occurance distribution */
delay_per_stddev = 0.1	    /* Stdev of slowdown occurance distribution */

=============
Compiling
=============
Copy this directory into the source directory of your code, and then copy
the following lines into your Makefile

# Copy the following lines to your makefile:
MAKES = <makefile-name>
ILIBS = timerInterrupter.o interrupter_utils.o rantools.o -lm
FLAGS = <Choice from above>
rantools.o: iutils/rantools.h iutils/rantools.c $(MAKES)
	cc $(FLAGS) -c iutils/rantools.c

interrupter_utils.o: iutils/interrupter_utils.h iutils/interrupter_utils.c $(MAKES)
	cc $(FLAGS) -c iutils/interrupter_utils.c

timerInterrupter.o: iutils/timerInterrupter.c iutils/interrupter.h $(MAKES)
        cc $(FLAGS) -c iutils/timerInterrupter.c

Then just link your source with $(ILIBS) when you compile.

===================
Using the API
===================
In addition to compiling,  you'd like to benchmark with noise, you'll need to 
include a few files and call the relevent functions.  Note the 
compiler directives above which additionally determine the 
behavior of these functions.
+------------------------------------------------------------------------------------
| <Initialization of Your Code>
| ...
| #incude "iutils/interrupter.h"
| //Initialization code
| initInterrupter();
| 
| //This file is necessary only for the readConfigInterrupter() function
| #include "iutils/interrupter_utils.h"
| readConfigInterrupter(config_file,parity,iLimit,&sync,
| 	delay_dur,delay_per_mean,delay_per_stddev);
| 
| setInterrupterParameters(processor_id,rng_seed, delay_dur, delay_per_mean,
| 			       delay_per_stddev,iLimit,parity,sync);
| ...
| <Barrier>
| ...
| //Start and stop synthetic noise.  Also times execution.
| startInterrupter();
| ...
| <Main body of Your Code>
| ...
| stopInterrupter();
| ...
| <Finalize Your Program>
+------------------------------------------------------------------------------------

=============
Contributing
=============
This project is built to be used as a git-submodule in some larger
code base (while there are many workarounds to this).  Be sure to 
read up on them before advancing the utilities herein, and thank
you in advance for your interest:
http://git-scm.com/book/en/Git-Tools-Submodules

==============================
Testing
==============================
interrupttest: timerInterrupter.o iutils/interrupttest.c $(MAKES)
	$(MPICC) $(FLAGS) -o interrupttest iutils/interrupttest.c timerInterrupter.o

If you'd like to test that your machine can handle an interrupter, be sure to include
the above target in your make file and read the short documentation in 
iutils/interrupttest.c

==============================
Note On Unmainted Utilities
==============================
You'll notice that there are a number of other files in here that aren't mentioned
in the README, and aren't even used in the reference code we give.  These include
amongst them:
	threadInterrupter
	signalInterrupter
		signals
		resolution

These interrupters in general are not as portable as the timerInterrupter, and so their 
development has fallen by the wayside.  The files signals and resolution are to be used
for testing along with the signalInterrupter facilities.