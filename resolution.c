/* Compute time resolution of SIGSTOP / SIGSTART.
 *
 * Author: Stephen F. Siegel
 * Last modified: 04-Nov-2012
 *
 * The main process forks a child process.  The main process then does
 * some work, which consists of adding a bunch of integers.  The child
 * process repeatedly interrupts the main process.  An interruption
 * consists of sending signal SIGSTOP, waiting for some duration, and
 * then sending signal SIGSTART.  The child does this n times.  By
 * doing this once with n=0, and then again with n some high number,
 * we can compare the times and estimate the overhead time consumed by
 * one interrupt.
 */
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <sys/types.h>

#define MILLION 1000000
#define BILLION 1000000000

/* Number of iterations of to do for "work" by main process */
#define NITERS (2*BILLION)

/* The number of interruptions */
#define NINTERRUPTS 200

/* The time between interrupts, in nanoseconds.  Must be less than
 * 10^9.
 *
 * Example: (20*MILLION) = 20 milliseconds */
#define INTERRUPT_PERIOD (20*MILLION)

/* The duration of an interrupt (time between sending stop and start
 * signals) in nanoseconds.  Must be less than 10^9.
 *
 *  Example: (4*MILLION) = 4 milliseconds
 */
#define INTERRUPT_DURATION (2*MILLION)

int sum = 0; // for doing "work"

/* Measure difference between two time values, returning answer in
 * seconds as double. */
double delta(struct timeval *tp1, struct timeval *tp2) {
  time_t sec1 = tp1->tv_sec, sec2 = tp2->tv_sec;
  suseconds_t microsec1 = tp1->tv_usec, microsec2 = tp2->tv_usec;

  if (microsec2 > microsec1) {
    microsec1 += 1000000;
    sec1--;
  }
  return ((double)(1000000*(sec1-sec2) + microsec1 - microsec2))/1000000.0;
}

void dowork() {
  int i;
  
  sum = 0;
  for (i = 0; i < NITERS; i++) sum += i%100;
}

double runMainProcess() {
  struct timeval current_time, previous_time;
  
  gettimeofday(&previous_time, NULL);
  dowork();
  gettimeofday(&current_time, NULL);
  return delta(&current_time, &previous_time);
}

void runInterrupterProcess(int ninterrupts) {
  int parent = getppid(); // where to send the signal
  struct timespec interrupt_period; // time until next interrupt
  struct timespec remaining_time; // not used
  struct timespec interrupt_duration; // duration of the interrupt
  int i;

  interrupt_period.tv_sec = 0;
  interrupt_period.tv_nsec = INTERRUPT_PERIOD;
  interrupt_duration.tv_sec = 0;
  interrupt_duration.tv_nsec = INTERRUPT_DURATION;
  for (i=0; i < ninterrupts; i++) {
    int err;
      
    nanosleep(&interrupt_period, &remaining_time);
    err = kill(parent, SIGSTOP);
    if (err) {
      printf("Interrupter failed to send SIGSTOP with error code %d\n",
	     err);
      fflush(stdout);
    }
    nanosleep(&interrupt_duration, &remaining_time);
    err = kill(parent, SIGCONT);
    if (err) {
      printf("Interrupter failed to send SIGCONT with error code %d\n",
	     err);
      fflush(stdout);
    }
  }
  printf("Interrupter done (ninterrupts=%d).\n", ninterrupts);
  fflush(stdout);
}

double run(int ninterrupts) {
  pid_t child = fork();

  if (child == 0) {
    runInterrupterProcess(ninterrupts);
    _exit(0);
  } else {
    double time = runMainProcess();
    int status;
    pid_t result = waitpid(child, &status, 0);

    if (result != child || status != 0) {
      fprintf(stderr, "Improper termination of child: err=%d, status=%d\n",
	      result, status);
      fflush(stderr);
      exit(1);
    }
    return time;
  }
}

int main(int argc, char *argv[]) {
  /* Total time spent by interruptor process between interrupts... */
  double total_period =
    (double)NINTERRUPTS*(double)INTERRUPT_PERIOD/BILLION;

  /* The expected difference between the uninterrupted and interrupted
   * execution times (if there were no overhead)... */
  double expected_diff =
    (((double)NINTERRUPTS)*((double)INTERRUPT_DURATION))/BILLION;

  /* The measured difference between those two times, in seconds... */
  double difference;

  /* difference - expected_diff ... */
  double overhead;

  /* The overhead time per interrupt, i.e.,  overhead/NINTERRUPTS.
   * Measured in seconds. */
  double tpi;

  /* Time of uninterrupted, interrupted executions (in seconds) ... */
  double time1, time2;

  time1 = run(0);
  printf("Uninterrupted execution done: sum=%d, time=%.6es\n", sum, time1);
  fflush(stdout);
  if (1.1*total_period > time1) {
    fprintf(stderr,
	    "Interruptions may exceed normal running time: %.3f > %.3f\n"
	    "Lower NINTERRUPTS or INTERRUPT_PERIOD, or increase NITERS\n",
	    1.1*total_period, time1);
    fflush(stderr);
    exit(1);
  }
  time2 = run(NINTERRUPTS);
  difference = time2 - time1;
  overhead = difference - expected_diff;
  tpi = overhead/NINTERRUPTS;
  printf("Interrupted execution done:   sum=%d, time=%.6es\n",
	 sum, time2);
  printf("  number of interruptions = %d\n", NINTERRUPTS);
  printf("  interrupt duration      = %.6es\n", (double)INTERRUPT_DURATION/BILLION);
  printf("  expected difference     = %.6es\n", expected_diff);
  printf("  measured difference     = %.6es\n", difference);
  printf("  total overhead          = %.6es\n", overhead);
  printf("  overhead per interrupt  = %.6es\n", tpi);
  fflush(stdout);
  return 0;
}
