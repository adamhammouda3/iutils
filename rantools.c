#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<assert.h>

#include "rantools.h"
#define ranf() rand()/RAND_MAX


/*
 *  compute a gaussian random number of mean m and stddev s 
 *  must set seed separately with srand().
 *  This is the Marsaglia polar method (I believe).
 */
double rng(double m, double s){
  double x1, x2, w, y1;
  
  do {
    x1 = 2.0 * ranf() - 1.0;
    x2 = 2.0 * ranf() - 1.0;
    w = x1 * x1 + x2 * x2;
  } while ( w == 0.0 || w >= 1.0 );
    
  
  w = sqrt( (-2.0 * log( w ) ) / w );
  y1 = x1 * w;
  return ( m + y1 * s );
}

/*
 *  compute a buffer of n guassian random numbers
 *  using box-muller transformation
 *  memory allocated internally. follow by free_rnglst 
 */
double *rnglst(double m, double s, int n){
  int i;
  double *ranbuf;
  //assert( 
  ranbuf = (double*)malloc( sizeof(double)*n );
  double num;
  for (i=0;i<n;++i){
    ranbuf[i] = rng(m,s);
    //while ( isnan(num) || isnan((-1*num)) ){ /* Can we produce double rands instead */
    //num = rng(m,s);                        /* And avoid this problem */
    //
    //}
  }
  return ranbuf;
}

/* 
 * free memory allocated in rnglst function
 */
void free_rnglst(double *list){
  free(list);
}


/*int main(){
  double step=0.001863;
  int i, n=60;
  srand(time(NULL));
  float x = rng(0.0,1.0);
  printf("x: %f\n", x);
  float *y = rnglst((30.0*step),(60.0*step),n);
  for (i=0;i<n;++i)
    printf("%f\n ", y[i]);
  free_rnglst(y);
  return 0;
}

*/
