/* signalInterrupter: implementation of interrupter.h using forked
 * process and signals
 *
 * Author: Stephen F. Siegel
 * Last modified: 05-Nov-2012
 *
 */
#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <signal.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>   /* for S_xxx file mode constants */
#include <sys/mman.h>   
#include <sys/wait.h>
#include <semaphore.h> 

#include "rantools.h"
#include "interrupter.h"

/* START is used in a message sent from parent to child meaning
 * "start sending me interrupts. */
#define START 1

/* Response to START.  Send from child to parent. */
#define READY 2

/* 10^6 */
#define MILLION 1000000

/* 10^9 */
#define BILLION 1000000000

static const double rand_range = (double)RAND_MAX + 1.0;

static int id;
static int numInterrupts = 0; /*Num interrupts over whole simulation */
static int done = 0;
static int seed;
static double duration;
static double mean;
static double stddev;
static float *ranbuf; /* list of pre-computed gaussian randoms */
static size_t nbuf;   /* size of gaussian random list */
static size_t next;   /* pointer to current index in ranbuf */

static pid_t child;
static pid_t parent;

static int down[2]; // pipe from parent to child
static int up[2]; // pipe from child to parent

static int downBuffer;
static int upBuffer;
static struct timeval start_tv, stop_tv;

static int nsteps;
static int job_type;

static int iLimit = -1;
static int parity = -1;

/* Measure difference between two time values, returning answer in
 * seconds as double. */
static double delta(struct timeval *tp1, struct timeval *tp2) {
  time_t sec1 = tp1->tv_sec, sec2 = tp2->tv_sec;
  suseconds_t microsec1 = tp1->tv_usec, microsec2 = tp2->tv_usec;

  if (microsec2 > microsec1) {
    microsec1 += MILLION;
    sec1--;
  }
  return ((double)(MILLION*(sec1-sec2) + microsec1 - microsec2))/1000000.0;
}

/* Sets the components of the time spec pointed to by ts
 * to correspond to the double value time. */
static void setTimeSpec(double time, struct timespec *ts) {
  long numSeconds = (int)time;
  
  ts->tv_sec = numSeconds;
  ts->tv_nsec = (long)((time - numSeconds)*BILLION);
}


static void sendDown(void *buf, int size) {
  int rc = write(down[1], buf, size);

  if (rc != size) {
    fprintf(stderr, "Proc %d: failed to send to interrupter: rc=%d\n",
	    id, rc);
    fflush(stderr);
    exit(1);
  }
}

static void sendUp(void *buf, int size) {
  int rc = write(up[1], buf, size);

  if (rc != size) {
    fprintf(stderr, "Interrupter %d: failed to send to parent: rc=%d\n",
	    id, rc);
    fflush(stderr);
    exit(1);
  }
}

static void recvFromAbove(void *buf, int size) {
  int rc;
  if (down[0]){
    rc = read(down[0], buf, size);
  } else
    fprintf(stderr, "Nothing sent yet!\n");
  
  if (rc != size) {
    fprintf(stderr, "Interrupter %d: failed to receive from parent: rc=%d\n",
	    id, rc);
    fflush(stderr);
    exit(1);
  }
}

static void recvFromBelow(void *buf, int size) {
  int rc = read(up[0], buf, size);
  
  if (rc != size) {
    fprintf(stderr, "Proc %d: failed to receive from interrupter: rc=%d\n",
	    id, rc);
    fflush(stderr);
    exit(1);
  }
}


static float rangauss(float m, float s){        
  float x1, x2, w, y1;
  
  do {
    x1 = 2.0 * ranf() - 1.0;
    x2 = 2.0 * ranf() - 1.0;
    w = x1 * x1 + x2 * x2;
  } while ( w >= 1.0 );
    
  w = sqrt( (-2.0 * log( w ) ) / w );
  y1 = x1 * w;
  
  return( m + y1 * s );
}

/* Interrupter process is stopped by sending it the SIGTERM signal */
void stopInterrupter() {
  int rc;
  gettimeofday(&stop_tv, NULL);
  rc = kill(child, SIGTERM);
  
  if (rc) {
    fprintf(stderr,
	    "Proc %d: unable to send SIGTERM to interrupter: err=%d\n",
	    id, rc);
    fflush(stderr);
    exit(rc);
  }
  rc = read(up[0], &upBuffer, sizeof(int));
  if (rc != sizeof(int)) {
    fprintf(stderr, "Proc %d: received %d bytes on up channel\n",
	    id, rc);
    fflush(stderr);
    exit(1);
  }
  numInterrupts = upBuffer;
  close(up[0]);
  close(down[1]);
}

/* Signal handling function for child (interrupter).
 * Used to handle signal SIGTERM from parent.
 * This signal means "stop sending me interrupts and quit."
 * The number of interrupts is sent to the parent on the
 * up pipe.
 */
static void handler(int signum) {
  int rc;
  if (signum != SIGTERM) {
    fprintf(stderr,
	    "Interrupter %d: caught unxpected signal: %d\n",
	    id, signum);
    fflush(stderr);
    _exit(1);
  }
#ifdef DEBUG
  printf("Interrupter %d: done (numInterrupts=%d).\n",
	 id, numInterrupts);
  fflush(stdout);
#endif
  // in case the last signal sent was SIGKILL...
  kill(parent, SIGCONT);

  sendUp(&numInterrupts,sizeof(int));

  // is it wrong to do this before the pipe has been read?????
  close(down[0]);
  close(up[1]);
  _exit(0);
}

static void cont_handler(int signum){
  int rc;
  if (signum != SIGCONT) {
    fprintf(stderr,
	    "Interrupter %d: caught unxpected signal: %d\n",
	    id, signum);
    fflush(stderr);
    _exit(1);
  }
  if(numInterrupts > iLimit){
    rc = kill(child, SIGTERM);
    if (rc) {
      fprintf(stderr,
	      "Proc %d: unable to send SIGTERM to interrupter: err=%d\n",
	      id, rc);
      fflush(stderr);
      exit(rc);
    }
    rc = read(up[0], &upBuffer, sizeof(int));
    if (rc != sizeof(int)) {
      fprintf(stderr, "Proc %d: received %d bytes on up channel\n",
	      id, rc);
      fflush(stderr);
      exit(1);
    }
    numInterrupts = upBuffer;
    close(up[0]);
    close(down[1]);
    
    done=1;
  }
}

static void runInterrupter() {
  struct timespec interrupt_period; // time until next interrupt
  struct timespec remaining_time; // not used
  struct timespec interrupt_duration; // duration of the interrupt
  int rc;
  double period;
  int active;
  
  recvFromAbove(&id, sizeof(int));
  recvFromAbove(&seed, sizeof(int));
  srand(seed);
  recvFromAbove(&duration, sizeof(double));
  recvFromAbove(&mean, sizeof(double));
  recvFromAbove(&stddev, sizeof(double));
  recvFromAbove(&parity, sizeof(int));
  recvFromAbove(&iLimit, sizeof(int));
  
  active = (id+1)%parity == 0;
  
#ifdef DEBUG
  printf("Interrupter %d starting.\n", id);
  fflush(stdout);
#endif
#ifdef SETRAND
  recvFromAbove(&nbuf, sizeof(int));
#endif
  setTimeSpec(duration, &interrupt_duration);  
  recvFromAbove(&downBuffer, sizeof(int));
  if (downBuffer != START) {
    fprintf(stderr,
	    "Interrupter %d received unknown code %d\n",
	    id, downBuffer);
    fflush(stderr);
    exit(1);
  }
  // first period selected from constant disrtibution so
  // starting point is evenly distributed...
  period =  2*mean*((double)rand())/rand_range;
#ifdef SETRAND
  ranbuf = rnglst(mean,stddev,nbuf);
#endif
  upBuffer = READY;
  sendUp(&upBuffer, sizeof(int));
  int i; 
  int j=0;
  sleep(90);
  while (active) {
    double r;
    setTimeSpec(period, &interrupt_period);
#ifdef DEBUG
    printf("Interrupter %d: sleeping for %.3es...\n", id, period);
    fflush(stdout);
#endif
    /* Child sleeps while parent uninterrupted */
    rc = nanosleep(&interrupt_period, &remaining_time);
    if (rc) {
      // where is errno???  check it is EINTR.
      handler(SIGTERM);
    }
#ifdef DEBUG
    printf("Interrupter %d: sending SIGSTOP to MPI process...\n", id);
    fflush(stdout);
#endif
    
#ifndef NOINTERRUPTS
    
    rc = kill(parent, SIGSTOP);
    if (rc) {
      fprintf(stderr,
	      "Interrupter %d failed to send SIGSTOP with error code %d\n",
	      id, rc);
      fflush(stderr);
      exit(rc);
    }
#endif
    /* Child sleeps after interrupting parent */
    rc = nanosleep(&interrupt_duration, &remaining_time);
    if (rc) {
      handler(SIGTERM);
    }
#ifdef DEBUG
    printf("Interrupter %d: sending SIGCONT to MPI process...\n", id);
    fflush(stdout);
#endif

#ifndef NOINTERRUPTS
    
    numInterrupts++;
    if (numInterrupts>=iLimit){
      active=0;
    }
    rc = kill(parent, SIGCONT);
    if (rc) {
      fprintf(stderr,
	      "Interrupter %d failed to send SIGCONT with error code %d\n",
	      id, rc);
      fflush(stderr);
      exit(rc);
    }
    
#ifdef ILOG    
    logInterrupt(MPI_Wtime());
#endif
    
#ifdef SETRAND
    if (next < (nbuf-1)){
      period = ranbuf[next++];
    } else {
      next =0;
      period = ranbuf[next];
    }
#else
    r = 0.0;
    for (i=0; i<12; i++)
      r += ((double)rand())/rand_range;      /*See if we can store this in buffer*/
    // r-6 is approximately standard normal
    period = mean + stddev*(r-6.0);
    if (period < 0) period = 0.0;
#endif 
#endif /* FOR NOINTERRUPTS directive*/
  }/*End while(1)*/
#ifdef DEBUG
  fprintf(stderr,"Proc %d outside of core ints @ %d ints",id,numInterrupts);
#endif
  sleep(5);
  volatile int x = 1;
  while(x){
    ;;
  }
}

/* Initializes the interrupter, specifying:
 * 
 * (1) an id number used for debugging
 * (2) a seed for the random number generator
 * (3) the duration (in seconds) of each interrupt
 * (4) the mean time between interrupts, and
 * (5) the standard deviation in the normal distribution of time between
 *     interrupts.
 *
 * This function creates the interrupter process using fork.
 * The interrupter process will not start running until it
 * receives the START code.
 * struct sigaction under the hood:
 * struct sigaction {
 *    void     (*sa_handler)(int);
 *    void     (*sa_sigaction)(int, siginfo_t *, void *);
 *    sigset_t   sa_mask;
 *    int        sa_flags;
 *    void     (*sa_restorer)(void);
 *  };
 */
//void initInterrupter(int* _nsteps) {
void initInterrupter() {//int jt,int* _nsteps
  int err;
  struct sigaction sa;
  //struct sigaction sac;
  //job_type=jt;
#ifdef ILOG
  /*nsteps=*_nsteps;
  if(initSharedILog(&nsteps)!=0)
    fprintf(stderr,"Interrupter initialization failed!!\n");
  fprintf(stderr,"Shared log initialized with size=%d\n",nsteps);*/
#endif
  
#ifdef DEBUG
  printf("Initializing interrupter \n");
  fflush(stdout);
#endif
  err = pipe(down); /* Create pipe for communication from parent->child
		       down[0] - Read from child 
		       down[1] - Write to child */
  if (err) {
    fprintf(stderr,
	    "Proc %d: error occurred in opening down pipe: err=%d\n",
	    id, err);
    fflush(stderr);
    exit(err);
  }
  err = pipe(up); /* Create a pipe from Child->Parent */
  if (err) {
    fprintf(stderr,
	    "Proc %d: error occurred in opening up pipe: err=%d\n",
	    id, err);
    fflush(stderr);
    exit(err);
  }
  sa.sa_handler = handler;
  sigemptyset(&sa.sa_mask);
  sa.sa_flags = 0;
  err = sigaction(SIGTERM, &sa, NULL);
  if (err) {
    fprintf(stderr,
	    "Proc %d: failed to register signal action: err=%d\n",
	    id, err);
    fflush(stderr);
    exit(err);
  }
  /*sac.sa_handler = cont_handler;
  sigemptyset(&sac.sa_mask);
  sac.sa_flags = 0;
  err = sigaction(SIGCONT, &sac, NULL);
  if (err) {
    fprintf(stderr,
	    "Proc %d: failed to register signal action: err=%d\n",
	    id, err);
    fflush(stderr);
    exit(err);
    }*/
  child = fork();
  if (child == 0) { 
    parent = getppid();
    close(up[0]);         /* Close Read end of Child->Parent pipe */
    close(down[1]);       /* Close Write end of Parent->Child pipe */
    runInterrupter();     /* Child process interrupts parent process */
  } else {          // main proc
    close(up[1]);         /* Close write end of child->Parent pipe */
    close(down[0]);       /* Close read end of parent->child pipe */
  }
}

void setInterrupterParameters(int _id, int _seed, double _duration,
			      double _mean, double _stddev, size_t _nbuf,
			      int _iLimit, int _parity){
  int rc;
#ifdef DEBUG
  printf("Setting interrupter parameters: id=%d, seed=%d, "
	 "duration=%.3es, mean=%.3es, stddev=%.3es, nbuf=%zd\n",
	 _id, _seed, _duration, _mean, _stddev, _nbuf);
  fflush(stdout);
#endif
  id = _id;
  seed = _seed;
  duration = _duration;
  mean = _mean;
  stddev = _stddev;
  iLimit=_iLimit;
  parity = _parity;
  
  /* Set an ilimit perhaps.  (Like, break after this many interrupts?? */

  sendDown(&id, sizeof(int));
  sendDown(&seed, sizeof(int));
  sendDown(&duration, sizeof(double));
  sendDown(&mean, sizeof(double));
  sendDown(&stddev, sizeof(double));
  sendDown(&parity, sizeof(int));
  sendDown(&iLimit, sizeof(int));
  
#ifdef SETRAND   /*Set global rng variables and send them to child process */
  next = 0;      /*Needed to initialize the random buffer in runInterrupter()*/
  nbuf = _nbuf;
  sendDown(&nbuf,sizeof(int));
#endif
}

/* Interrupter process already exists but is waiting for a message
 * on the down pipe.  This function sends it the START message. */
void startInterrupter() {
  int rc;
  downBuffer = START;
  gettimeofday(&start_tv, NULL);
  sendDown(&downBuffer, sizeof(int));
  recvFromBelow(&upBuffer, sizeof(int)); // READY
  if (upBuffer != READY) {
    fprintf(stderr, "Proc %d: expected READY from interrupter but received %d\n",
	    id, upBuffer);
    fflush(stderr);
    exit(1);
  }
}

double getDuration() {
  return duration;
}

double getMean() {
  return mean;
}

double getStddev() {
  return stddev;
}

/* Return total number of interrupts */
int getNumInterrupts() {
  return numInterrupts;
}

/* Returns total time of interrupts */
double getTotalInterruptTime() {
  return numInterrupts*duration;
}

/* Returns total time, in seconds between call to startInterrupter
 * and call to stopInterrupter. */
double getTotalTime() {
  return delta(&stop_tv, &start_tv);
}

void cleanInterrupter(){
  free(ranbuf);
}
