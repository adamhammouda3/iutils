#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "interrupter.h"

#define MILLION 1000000
#define BILLION 1000000000

#define NITERS (BILLION)

int nprocs, rank; // num procs and my MPI rank
int sum;

/* Measure difference between two time values, returning answer in
 * seconds as double. */
double delta(struct timeval *tp1, struct timeval *tp2) {
  time_t sec1 = tp1->tv_sec, sec2 = tp2->tv_sec;
  suseconds_t microsec1 = tp1->tv_usec, microsec2 = tp2->tv_usec;

  if (microsec2 > microsec1) {
    microsec1 += 1000000;
    sec1--;
  }
  return ((double)(1000000*(sec1-sec2) + microsec1 - microsec2))/1000000.0;
}

/* Print usage information and exit with error code 1 */
void quit() {
  printf("Usage: interrupttest DURATION PERIOD_MEAN PERIOD_STDDEV\n");
  printf("  DURATION: duration of each interrupt in seconds (double)\n");
  printf("  PERIOD_MEAN: mean time between interrupts in seconds (double)\n");
  printf("  PERIOD_STDDEV: standard deviation in period distribution (double)\n");
  fflush(stdout);
  exit(1);
}

void parseDouble(char *string, double *value) {
  int rc = sscanf(string, "%lf", value);
  
  if (rc != 1) quit();
}

void work() {
  int i;
  
  sum = 0.0;
  for (i=0; i<NITERS; i++) {
    sum = (sum+i)%123;
  }
}

// arguments: duration, mean, stddev
int main(int argc, char *argv[]) {
  double duration, mean, stddev; // the inputs
  struct timeval current_time, previous_time; // used to time things
  
  /* Number of interrupts that occurred */
  int numInterrupts;

  /* The expected difference between the uninterrupted and interrupted
   * execution times (if there were no overhead)... */
  double expected_diff;
  
  /* The measured difference between those two times, in seconds... */
  double difference;
  
  /* difference - expected_diff ... */
  double overhead;
  
  /* The overhead time per interrupt, i.e.,  overhead/NINTERRUPTS.
   * Measured in seconds. */
  double tpi;

  /* Time of uninterrupted, interrupted executions (in seconds) ... */
  double time1, time2;

  size_t nbuf = 100;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  if (rank == 0) {
    if (argc != 4) quit();
    parseDouble(argv[1], &duration);
    parseDouble(argv[2], &mean);
    parseDouble(argv[3], &stddev);
  }
  MPI_Bcast(&duration, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&mean, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(&stddev, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  gettimeofday(&previous_time, NULL);
  work();
  gettimeofday(&current_time, NULL);
  time1 = delta(&current_time, &previous_time);
  printf("Proc %d: Uninterrupted execution done: sum=%d, time=%.6es\n",
	 rank, sum, time1);
  fflush(stdout);
  
  initInterrupter();
  //clock()
  setInterrupterParameters(rank, (1+rank), duration, mean, stddev,50,10000,1);
  
  gettimeofday(&previous_time, NULL);
  startInterrupter();
  work();
  stopInterrupter();
  gettimeofday(&current_time, NULL);
  time2 = delta(&current_time, &previous_time);
  difference = time2 - time1;
  expected_diff = getTotalInterruptTime();
  overhead = difference - expected_diff;
  numInterrupts = getNumInterrupts();
  tpi = overhead/numInterrupts;
  printf("Proc=%d: Interrupted execution done:   sum=%d, time=%.6es\n",
	 rank, sum, time2);
  printf("Proc=%d:   number of interruptions = %d\n", rank, numInterrupts);
  printf("Proc=%d:   interrupt duration      = %.6es\n", rank, duration);
  printf("Proc=%d:   expected difference     = %.6es\n", rank, expected_diff);
  printf("Proc=%d:   measured difference     = %.6es\n", rank, difference);
  printf("Proc=%d:   total overhead          = %.6es\n", rank, overhead);
  printf("Proc=%d:   overhead per interrupt  = %.6es\n", rank, tpi);
  fflush(stdout);
  MPI_Finalize();
  return 0;
}
